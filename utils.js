function createTodo(title, status) {
  const todo = document.createElement('article');
  todo.classList.add('todo');

  const todoTitle = document.createElement('h1');
  todoTitle.classList.add('todo__title');
  todoTitle.textContent = title;

  const todoStatus = document.createElement('div');
  todoStatus.classList.add('todo__status');
  if(status) {
    todoStatus.textContent = 'Выполнено';
    todoStatus.classList.add('todo__status--completed');
  } else {
    todoStatus.textContent = 'В процессе';
  }

  todo.appendChild(todoTitle);
  todo.appendChild(todoStatus);

  return todo;
}

function createPost(title, content) {
  const post = document.createElement('article');
  post.classList.add('todo');

  const postTitle = document.createElement('h1');
  postTitle.classList.add('todo__title');
  postTitle.textContent = title;
  post.appendChild(postTitle);

  if(content) {
    const postContent = document.createElement('p');
    postContent.textContent = content;
    post.appendChild(postContent)
  }

  return post;
}

function createModal(node) {
  const modal = document.createElement('div');
  modal.classList.add('modal');
  const modalContent = document.createElement('div');
  modalContent.classList.add('modal__content');
  if(node) {
    modalContent.appendChild(node);
  }
  modal.addEventListener('click', function() {
    modal.remove();
  })
  modal.appendChild(modalContent);

  return modal;
}
